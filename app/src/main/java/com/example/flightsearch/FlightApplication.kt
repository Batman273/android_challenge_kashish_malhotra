package com.example.flightsearch

import com.example.flightsearch.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * We create a custom Application class that extends DaggerApplication.
 * We then override applicationInjector() which tells Dagger how to make our @Singleton Component
 */

open class FlightApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<FlightApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}