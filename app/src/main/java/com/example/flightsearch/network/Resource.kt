package com.example.flightsearch.network

class Resource<out T>(val status: Status, val data: T?, val error: Error?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as Resource<*>?

        return if (status != resource?.status) {
            false
        } else (if (error?.message != null) error.message == resource.error?.message else resource.error?.message == null)
                && if (data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (data?.hashCode() ?: 0)
        result = 31 * result + (error?.message?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Status = $status | Data = $data | Error = $error"
    }


    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(error: Error? = null, data: T? = null): Resource<T> {
            return Resource(Status.ERROR, data, error)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }

}