package com.example.flightsearch.network

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class FlightData(
    @SerializedName("appendix")
    val appendix: Appendix? = null,

    @SerializedName("flights")
    var flights: List<Flight>? = null
)

data class Appendix(

    @SerializedName("airlines")
    var airlines: JsonObject? = null,

    @SerializedName("airports")
    var airports: JsonObject? = null,

    @SerializedName("providers")
    var providers: JsonObject? = null
)

data class Flight(

    @SerializedName("originCode")
    val originCode: String,

    @SerializedName("destinationCode")
    val destinationCode: String,

    @SerializedName("departureTime")
    val departureTime: Long,

    @SerializedName("arrivalTime")
    val arrivalTime: Long,

    @SerializedName("fares")
    var fares: List<Fare> = listOf(),

    @SerializedName("airlineCode")
    val airlineCode: String,

    @SerializedName("class")
    val flightClass: String,

    var minFare: Int = 0,

    var isOpened: Boolean = false
)

data class Fare(

    @SerializedName("providerId")
    var providerId: Int,

    @SerializedName("fare")
    var fare: Int
)
