package com.example.flightsearch.network

import retrofit2.Call
import retrofit2.http.GET

interface FlightSearchService {

    @GET(value = "v2/5979c6731100001e039edcb3")
    fun getFlights(): Call<FlightData>
}