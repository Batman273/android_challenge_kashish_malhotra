package com.example.flightsearch.di.module

import com.example.flightsearch.home.view.SearchFragment
import com.example.flightsearch.di.scopes.FragmentScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeActivityModule{

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}