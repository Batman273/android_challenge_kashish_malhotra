package com.example.flightsearch.di

import android.app.Application
import com.example.flightsearch.FlightApplication
import com.example.flightsearch.di.module.ActivityBindingModule
import com.example.flightsearch.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Application level Component that binds the Application context.
 */

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, ActivityBindingModule::class,
        AppModule::class]
)
interface AppComponent : AndroidInjector<FlightApplication> {

    /**
     * Application will just be provided into our app graph now.
     */
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}