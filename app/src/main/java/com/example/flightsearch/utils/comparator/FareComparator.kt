package com.example.flightsearch.utils.comparator

import com.example.flightsearch.network.Fare

/**
 * Custom comparator to sort Fare object on the basis of fare value
 */
class FareComparator : Comparator<Fare> {

    override fun compare(fare1: Fare?, fare2: Fare?): Int {
        if (fare1 == null || fare2 == null)
            return 0
        return fare1.fare.compareTo(fare2.fare)
    }

}