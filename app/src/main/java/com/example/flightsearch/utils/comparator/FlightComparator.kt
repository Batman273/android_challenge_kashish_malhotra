package com.example.flightsearch.utils.comparator

import com.example.flightsearch.home.model.SortOption
import com.example.flightsearch.network.Flight

/**
 * Custom comparator to sort Flight object on the basis of minimum fare value
 */
class FlightComparator(private val sortOption: SortOption) : Comparator<Flight> {

    override fun compare(flight1: Flight?, flight2: Flight?): Int {
        if (flight1 == null || flight2 == null)
            return 0
        return when (sortOption) {
            SortOption.PRICE -> {
                flight1.minFare.compareTo(flight2.minFare)
            }
            SortOption.DEPARTURE_TIME -> {
                flight1.departureTime.compareTo(flight2.departureTime)

            }
            SortOption.ARRIVAL_TIME -> {
                flight1.arrivalTime.compareTo(flight2.arrivalTime)

            }
        }
    }
}