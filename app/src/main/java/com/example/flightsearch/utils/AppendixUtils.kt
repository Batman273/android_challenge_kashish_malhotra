package com.example.flightsearch.utils

import com.example.flightsearch.network.Appendix
import com.google.gson.JsonObject

object AppendixUtils {

    private var airlinesJson: JsonObject? = null
    private var airportsJson: JsonObject? = null
    private var providersJson: JsonObject? = null

    fun setData(appendix: Appendix?) {
        this.airlinesJson = appendix?.airlines
        this.airportsJson = appendix?.airports
        this.providersJson = appendix?.providers
    }

    fun getAirlineValue(key: String): String {
        return airlinesJson?.let {
            if (it.has(key)) it.get(key).asString else ""
        } ?: ""
    }

    fun getAirportValue(key: String): String {
        return airportsJson?.let {
            if (it.has(key)) it.get(key).asString else ""
        } ?: ""
    }

    fun getProviderValue(key: String): String {
        return providersJson?.let {
            if (it.has(key)) it.get(key).asString else ""
        } ?: ""
    }
}