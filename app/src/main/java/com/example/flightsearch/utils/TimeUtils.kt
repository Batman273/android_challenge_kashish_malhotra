package com.example.flightsearch.utils

import java.text.SimpleDateFormat
import java.util.*

fun Long.getTime(): String {
    val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return formatter.format(calendar.time)
}

fun Long.getDuration(): String {
    val minutes = (this / 1000 / 60) % 60
    val hours = this / 1000 / 60 / 60
    return if (minutes == 0L)
        String.format("%dh", hours)
    else
        String.format("%dh %dm", hours, minutes)
}
