package com.example.flightsearch.home.view

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpaceItemDecorator(private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val pixelSpacing = (spacing * Resources.getSystem().displayMetrics.density).toInt()
        outRect.bottom = pixelSpacing
        outRect.left = pixelSpacing
        outRect.right = pixelSpacing
        if (parent.getChildAdapterPosition(view) == 0)
            outRect.top = pixelSpacing * 2
    }

}