package com.example.flightsearch.home.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flightsearch.R
import com.example.flightsearch.home.listener.FlightClickListener
import com.example.flightsearch.home.model.FlightClickData
import com.example.flightsearch.home.model.SortOption
import com.example.flightsearch.home.viewModel.SearchViewModel
import com.example.flightsearch.network.Flight
import com.example.flightsearch.network.Resource
import com.example.flightsearch.network.Status
import com.example.flightsearch.utils.AppendixUtils
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_search.*
import javax.inject.Inject

class SearchFragment : DaggerFragment(), FlightClickListener, View.OnClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var searchViewModel: SearchViewModel

    private var flightsAdapter = FlightsAdapter(this)

    private val itemObserver = Observer<FlightClickData> {
        flightsAdapter.notifyItemChanged(it.position, it)
    }

    private val dataObserver = Observer<Resource<List<Flight>>> {
        when (it.status) {
            Status.LOADING -> {
                btn_retry.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            }
            Status.SUCCESS -> {
                btn_retry.visibility = View.GONE
                it.data?.let { flights ->
                    setTopText(flights)
                    flightsAdapter.setData(flights)
                }
                progress_bar.visibility = View.GONE
            }
            Status.ERROR -> {
                Toast.makeText(
                    context,
                    getString(R.string.toast_something_went_wrong),
                    Toast.LENGTH_LONG
                ).show()
                btn_retry.visibility = View.VISIBLE
                progress_bar.visibility = View.GONE
            }
        }
    }

    private fun setTopText(flights: List<Flight>) {
        if (flights.isNotEmpty()) {
            tv_top.visibility = View.VISIBLE
            tv_top.text = getString(
                R.string.text_to, AppendixUtils.getAirportValue(flights[0].originCode),
                AppendixUtils.getAirportValue(flights[0].destinationCode)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchViewModel = ViewModelProviders.of(this, viewModelFactory)[SearchViewModel::class.java]
        setListener()
        setAdapter()
        observeData()
    }

    private fun setListener() {
        btn_retry.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.filter_menu, menu)
        menu.findItem(searchViewModel.selectedSortOption.id).isChecked = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    /**
     * Handling of sorting options from options menu
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_cheapest -> {
                if (!item.isChecked)
                    searchViewModel.sortList(SortOption.PRICE)
                item.isChecked = !item.isChecked
            }
            R.id.menu_departure -> {
                if (!item.isChecked)
                    searchViewModel.sortList(SortOption.DEPARTURE_TIME)
                item.isChecked = !item.isChecked
            }
            R.id.menu_arrival -> {
                if (!item.isChecked)
                    searchViewModel.sortList(SortOption.ARRIVAL_TIME)
                item.isChecked = !item.isChecked
            }
        }
        return true
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_retry -> searchViewModel.fetchFlightsData()
        }
    }

    private fun setAdapter() {
        rv_flights.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rv_flights.addItemDecoration(SpaceItemDecorator(12))
        rv_flights.adapter = flightsAdapter
    }

    /**
     * Observing live data exposed by searchViewModel
     */
    private fun observeData() {
        searchViewModel.getFlightsLiveData().observe(viewLifecycleOwner, dataObserver)
        searchViewModel.getFlightItemLiveData().observe(viewLifecycleOwner, itemObserver)
    }

    /**
     * Flight item click method
     */
    override fun onFlightClicked(position: Int) {
        searchViewModel.onItemClick(position)
    }
}