package com.example.flightsearch.home.repository

import com.example.flightsearch.network.FlightSearchService
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository to fetch data (in our case, only from api)
 */

@Singleton
class SearchRepository @Inject constructor(private val searchApiService: FlightSearchService) {

    fun getFlights() = searchApiService.getFlights()
}