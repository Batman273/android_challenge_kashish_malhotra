package com.example.flightsearch.home.model

import com.example.flightsearch.R

enum class SortOption(val id: Int) {
    PRICE(R.id.menu_cheapest), DEPARTURE_TIME(R.id.menu_departure), ARRIVAL_TIME(R.id.menu_arrival)
}

data class FlightClickData(
    val position: Int,
    val shouldOpen: Boolean
)