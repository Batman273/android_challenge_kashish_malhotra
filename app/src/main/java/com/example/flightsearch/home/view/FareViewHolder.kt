package com.example.flightsearch.home.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.flightsearch.R
import com.example.flightsearch.network.Fare
import com.example.flightsearch.utils.AppendixUtils
import kotlinx.android.synthetic.main.item_fare.view.*

class FareViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(fareData: Fare) {
        with(itemView) {
            tv_provider.text = AppendixUtils.getProviderValue(fareData.providerId.toString())
            tv_fare.text = context.getString(R.string.text_price, fareData.fare)
        }
    }
}