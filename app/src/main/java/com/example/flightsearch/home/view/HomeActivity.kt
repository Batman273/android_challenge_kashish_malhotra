package com.example.flightsearch.home.view

import android.os.Bundle
import com.example.flightsearch.R
import dagger.android.support.DaggerAppCompatActivity

class HomeActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        if (savedInstanceState == null) {
            //The Activity is being created for the first time, so create and
            //add new fragments.
            addSearchFragment()
        } else {
            //Otherwise, the activity is coming back after being destroyed.
            //The FragmentManager will restore the old Fragments so we don't
            //need to create any new ones here.
        }
    }

    private fun addSearchFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fl_home, SearchFragment())
        transaction.commit()
    }
}
