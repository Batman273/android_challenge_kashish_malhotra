package com.example.flightsearch.home.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.flightsearch.home.model.FlightClickData
import com.example.flightsearch.home.model.SortOption
import com.example.flightsearch.home.repository.SearchRepository
import com.example.flightsearch.network.Flight
import com.example.flightsearch.network.FlightData
import com.example.flightsearch.network.Resource
import com.example.flightsearch.utils.AppendixUtils
import com.example.flightsearch.utils.comparator.FareComparator
import com.example.flightsearch.utils.comparator.FlightComparator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    app: Application,
    private val searchRepository: SearchRepository
) : AndroidViewModel(app) {

    private val flightsData = MutableLiveData<Resource<List<Flight>>>()
    private var flightsList: MutableList<Flight>? = mutableListOf()
    private var flightItemLiveData = MutableLiveData<FlightClickData>()
    var selectedSortOption = SortOption.PRICE

    fun getFlightsLiveData(): LiveData<Resource<List<Flight>>> = flightsData

    fun getFlightItemLiveData(): LiveData<FlightClickData> = flightItemLiveData

    init {
        viewModelScope.launch(Dispatchers.IO) {
            fetchFlightsData()
        }
    }

    fun fetchFlightsData() {
        flightsData.postValue(Resource.loading())
        searchRepository.getFlights().enqueue(object : Callback<FlightData> {
            override fun onResponse(call: Call<FlightData>, response: Response<FlightData>) {
                if (response.isSuccessful) {
                    response.body()?.let { flightData ->
                        flightsList = getFlightsData(flightData)?.toMutableList()
                        AppendixUtils.setData(flightData.appendix)
                        sortList(selectedSortOption)
                        flightsData.postValue(Resource.success(flightsList))
                    } ?: flightsData.postValue(Resource.error())
                } else
                    flightsData.postValue(Resource.error())
            }

            override fun onFailure(call: Call<FlightData>, t: Throwable) {
                flightsData.postValue(Resource.error())
            }
        })
    }

    private fun getFlightsData(data: FlightData): List<Flight>? {
        data.flights?.map {
            val fares = it.fares.toMutableList()
            Collections.sort(fares, FareComparator())
            if (fares.size > 0) it.minFare = fares[0].fare
            it.fares = fares
        }
        return data.flights
    }

    /**
     * method to sort list, passing in on of the SortOption values [PRICE, DEPARTURE_TIME, ARRIVAL_TIME],
     * using custom comparator
     */
    fun sortList(sortOption: SortOption) {
        selectedSortOption = sortOption
        if (flightsList != null && flightsList!!.isNotEmpty())
            viewModelScope.launch(Dispatchers.Default) {
                flightsList?.let { list ->
                    Collections.sort(list, FlightComparator(sortOption))
                    flightsData.postValue(Resource.success(list))
                }
            }
    }

    fun onItemClick(position: Int) {
        flightsList?.let {
            it[position].isOpened = !it[position].isOpened
            flightItemLiveData.postValue(FlightClickData(position, it[position].isOpened))
        }
    }
}