package com.example.flightsearch.home.view

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flightsearch.R
import com.example.flightsearch.home.listener.FlightClickListener
import com.example.flightsearch.home.model.FlightClickData
import com.example.flightsearch.network.Flight
import com.example.flightsearch.utils.AppendixUtils
import com.example.flightsearch.utils.getDuration
import com.example.flightsearch.utils.getTime
import kotlinx.android.synthetic.main.item_flight.view.*

class FlightViewHolder(itemView: View, private val flightClickListener: FlightClickListener) :
    RecyclerView.ViewHolder(itemView) {

    private val fareAdapter = FaresAdapter()
    private val layoutManager = LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)

    fun bindState(flightClickData: FlightClickData) {
        if (flightClickData.shouldOpen)
            changeViewsVisibility(View.VISIBLE)
        else
            changeViewsVisibility(View.GONE)

    }

    fun bind(flight: Flight) {
        with(itemView) {
            tv_airline.text = AppendixUtils.getAirlineValue(flight.airlineCode)
            tv_class.text = flight.flightClass
            tv_start_time.text = flight.departureTime.getTime()
            tv_end_time.text = flight.arrivalTime.getTime()
            tv_duration.text = (flight.arrivalTime - flight.departureTime).getDuration()
            tv_min_price.text = context.getString(R.string.text_price, flight.minFare)

            rv_fares.layoutManager = layoutManager
            rv_fares.adapter = fareAdapter
            fareAdapter.setData(flight.fares)

            cl_flight.setOnClickListener {
                flightClickListener.onFlightClicked(adapterPosition)
            }

            if (flight.isOpened)
                changeViewsVisibility(View.VISIBLE)
            else
                changeViewsVisibility(View.GONE)

        }
    }

    private fun changeViewsVisibility(visibility: Int) {
        itemView.rv_fares.visibility = visibility
        itemView.tv_booking_options.visibility = visibility
    }
}