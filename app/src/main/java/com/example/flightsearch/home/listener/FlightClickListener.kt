package com.example.flightsearch.home.listener

interface FlightClickListener {

    fun onFlightClicked(position: Int)
}