package com.example.flightsearch.home.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.flightsearch.network.Fare
import com.example.flightsearch.R
import com.example.flightsearch.utils.AppendixUtils
import kotlinx.android.synthetic.main.item_fare.view.*

class FaresAdapter : RecyclerView.Adapter<FareViewHolder>() {

    private var flightsList = listOf<Fare>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FareViewHolder {
        return FareViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_fare,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return flightsList.size
    }

    override fun onBindViewHolder(holder: FareViewHolder, position: Int) {
        holder.bind(flightsList[position])
    }

    fun setData(flightsList: List<Fare>) {
        this.flightsList = flightsList
        notifyDataSetChanged()
    }
}