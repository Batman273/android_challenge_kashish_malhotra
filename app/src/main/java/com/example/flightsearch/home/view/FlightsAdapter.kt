package com.example.flightsearch.home.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flightsearch.network.Flight
import com.example.flightsearch.R
import com.example.flightsearch.home.listener.FlightClickListener
import com.example.flightsearch.home.model.FlightClickData
import com.example.flightsearch.utils.AppendixUtils
import com.example.flightsearch.utils.getDuration
import com.example.flightsearch.utils.getTime
import kotlinx.android.synthetic.main.item_flight.view.*

class FlightsAdapter(private val flightClickListener: FlightClickListener) :
    RecyclerView.Adapter<FlightViewHolder>() {

    private var flightsList = listOf<Flight>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
        return FlightViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_flight,
                parent, false
            ), flightClickListener
        )
    }

    override fun getItemCount(): Int {
        return flightsList.size
    }

    override fun onBindViewHolder(
        holder: FlightViewHolder, position: Int, payloads: MutableList<Any>
    ) {
        if (payloads.isNotEmpty()) {
            for (payload in payloads) {
                if (payload is FlightClickData)
                    holder.bindState(payload)
            }
        } else
            super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
        holder.bind(flightsList[position])
    }

    fun setData(flightsList: List<Flight>) {
        this.flightsList = flightsList
        notifyDataSetChanged()
    }
}